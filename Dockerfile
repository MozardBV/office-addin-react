FROM node:18-bullseye

LABEL Maintainer="Marten de Bruijn <m.debruijn@ienpm.nl>"
LABEL Description "Integratie tussen het Mozard zaaksysteem en Microsoft Office"

RUN mkdir -p /home/node/code/node_modules && chown -R node:node /home/node/code

WORKDIR /home/node/code

COPY package*.json ./

USER node

RUN npm ci --force

COPY --chown=node:node . .

RUN mkdir -p /home/node/code/node_modules/.cache/webpack-dev-server

RUN test -f /home/node/.office-addin-dev-certs/localhost.key && cat /home/node/code/.office-addin-dev-certs/localhost.key > /home/node/code/node_modules/.cache/webpack-dev-server/server.pem || echo "Using Webpack certificate"
RUN test -f /home/node/.office-addin-dev-certs/localhost.crt && cat /home/node/code/.office-addin-dev-certs/localhost.crt >> /home/node/code/node_modules/.cache/webpack-dev-server/server.pem || echo ""
RUN test -f /home/node/.office-addin-dev-certs/ca.crt && cat /home/node/code/.office-addin-dev-certs/ca.crt >> /home/node/code/node_modules/.cache/webpack-dev-server/server.pem || echo ""

EXPOSE 3000

CMD ["node_modules/.bin/webpack", "serve", "--mode", "development", "--server-type","https"]

HEALTHCHECK --timeout=300s CMD curl --silent --insecure --fail https://127.0.0.1:3000/
