#!/bin/bash
string_tags=$(git tag --list)
IFS=$'\n' tags=($string_tags)
# echo $my_string
# echo "#### Changelog 0.1.0" > changelog.md
# git log --oneline --decorate 0.1.0.. > changelog.md

echo "" > changelog.md

for i in "${!tags[@]}"
do
  echo "$tags[$i]"
  title="## Version ${tags[$i]}"
  if [ "$i" -eq "0" ];
  then
    commits=$(git log --pretty=tformat:"%s%n" "${tags[$i]}")
  else
    commits=$(git log --pretty=tformat:"%s%n" "${tags[$i-1]}..${tags[$i]}")
  fi

  echo "$title\n$commits\n\n$(cat changelog.md)" > changelog.md


  # git log --oneline --decorate $(tags[$i-1])..$(tags[$i]) > changelog.md
  # echo -e "task goes here\n$(cat changelog.md)" > changelog.md
done

echo "# Changelog\n\n$(cat changelog.md)" > changelog.md
