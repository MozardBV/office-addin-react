# Changelog

## Version 3.1.4
increase version

Merge branch 'update-depencencies'

update dependecies

alsow for tests

fix npm ci to npm ci --force

fix compose file

Merge commit '67e62fcc05c36b3af64ddf01a4138fbb7d08f639'

Merge commit 'ef7d46e916fa98550b6558699dadf66aee6c18c4'

excel can now reupload file

update dependencies

update dependencies

update dependencies

update dependencies

fix for navbar footer

Merge remote-tracking branch 'origin/main'

change manifest's and in app name

Upload New File

Add new directory

add localstorage modalOpened

the popup is first shown on startup

add button for modal Wat is Officeintergratie

add icon 64x64

Merge branch 'staging'

onherkende formatter

geen task op startup

Merge branch 'reset-marten' into 'main'

Revert "fix versturen van mail"

Revert "OutlookMailbox: handle promise rejection"

Revert "deprecated extensie uit recommended"

Revert "update readme"

fix versturen van mail

OutlookMailbox: handle promise rejection

deprecated extensie uit recommended

update readme

Merge branch 'z900923' into 'main'

Merge branch 'main' into 'z900923'

ondersteun gedeelde inbox

voegt validate npm scripts toe

geen automatic tasks

feat(outlook): support shared folders

style(outlook): manifest formatting

chore(vscode): geen auto tasks bij opstart

⏪️ revert: terug naar voor de updates 0d62329c

🧹 chore: ran npm audit fix

Merge branch 'main' of gitlab.com:MozardBV/office-addin-react

:arrow_up: chore: dependencies bijwerken

Merge branch 'z901218-nieuw-document' into 'main'

Z901218 nieuw document

Merge branch 'ie11' into 'main'

:checkered_flag: compatibility voor ie11

🧹 chore: dependencies bijwerken

## Version 3.1.3
🧹 chore: versie nr ophogen

Merge branch 'z899862' into 'main'

## Version 3.1.2
🧹 chore: versie ophogen

✨ feat(manifest): wijziging naam

## Version 3.1.1
👥 chore: repo overdragen

⬆️ chore: dependencies bijwerken

📌 chore: dependencies pinnen op npm audit

⬆️ chore: dependencies bijwerken

👽️ chore: wijzigingen in rendermethode in react 18

⬆️ chore: dependencies bijwerken

⬆️ chore: dependencies bijwerken

🔧 chore: automatische tasks toestaan

⬆️ chore: dependencies bijwerken

⬆️ chore: dependencies bijwerken

⬆️ chore: dependencies bijwerken

⬆️ chore: dependencies bijwerken

💚 ci: only vervangen met rules

⬆️ chore: dependencies bijwerken

⬆️ chore: dependencies bijwerken

Merge branch 'outlook-mac' into 'main'

🐛 fix(ViewMain): status code fix

Merge branch 'outlook-bugs' into 'main'

🔒️ fix: literal regex in webpack

📌 fix: dependency pinning na npm audit fix

🔒️ fix: gebruik Array.prototype.at waar mogelijk

🚨 chore: negeer semgrep op eenvoudige for loops

🔒 fix: gebruik regex literal

⬆️ chore: dependencies bijwerken

🐛 fix(outlookmailbox): email plus bijlagen fix

🐛 fix(outlookmailbox): bijlagen leesbaar maken

⬆️ chore: dependencies bijwerken

⬆️ chore: dependencies bijwerken

🚑 fix: react-app-polyfill gebruiken

🚑 fix: core-js en webpack-buildin niet transpilen

🚑 fix: node_modules door babel halen

➕ fix: react-is toevoegen als dependency

🚑 fix: webpack aanpassingen voor IE11

🚑 fix: transpile ook node_modules

🚑 fix: geen BigInt64 in output voor IE11

⬆️ chore: dependencies bijwerken

Merge branch 'ie11' into 'main'

🐛 fix(webpack): ie11 es5 ondersteuning

Merge branch 'foutmeldingen' into 'main'

🐛 fix: foutmeldingen controleren op status code

Merge branch 'main' of gitlab.com:MozardBV/office-addin-react

💚 ci: custom build job

🐛 fix(ViewMain): titel veranderd naar documentnaam

Merge branch 'weergavenaam-fix' into 'main'

🐛 fix(ViewMain): niet falen indien geen moz_zk_weergavenaam

⬆️ chore: dependencies bijwerken

⬆️ chore: dependencies bijwerken

🔧 chore: vscode docker extension toevoegen

Merge branch 'tynaarlo' into 'main'

🐛 fix(package-lock): fix dependencies met npm i

merge main

💚 ci: deploy alleen op main

💚 ci: environment url wegschrijven

🔧 chore: markdownlint als json

💚 ci: environment url als artifact

🔒️ feat: React strict mode

👽️ fix: gebruik Office.onReady() callback

💚 ci: geen experimentele fetch

💚 ci: build op node 16

merge main

💚 ci: geen experimentele fetch

🔧 chore: dependencies bijwerken

⬆️ chore: dependencies bijwerken

🚚 feat: direct middleware benaderen in proxy

🚨 style: linter warnings verhelpen

👽️ chore: deprecated property verwijderen

📄 chore: jaartal ophogen

📄 chore: jaartal ophogen

⬆️ chore: dependencies bijwerken

⬆️ chore: dependencies bijwerken

💎 style(outlookmailbox): verplaatsing fn naar private

🐛 fix(outlookmailbox): filter niet toegestane chars

✨ feat(outlookmailbox): toevoegen foutmelding office interne fout

🐛 fix(viewmain): toevoegen controle weergavenaam

✨ feat(outlookmailbox): toevoegen bijlagen functionaliteit

🧹 chore: toevoegen file-type en buffer

📝 docs(readme): hoofdlettergevoeligheid windows

⬇️ fix: downgrade naar node 16

⬆️ chore: dependencies bijwerken

comment met link naar office issue

✨feat zknaam weergeven + nieuwe foutmeldingen

👽️ fix: react router v6 syntax

♻️ refactor(ViewMain): setDocName in finally block

🔨fix standaard titel powerpoint weggehaald + vergeten return

🔨fix standaard titel alleen datum op reject

📝docs (README): link terminal flags inspector toegevoegd

✨feat standaardtitel excel

✨feat standaartitel datum-eerste karakters

✨feat (ViewMain): controle lengte doc naam

⬆️ chore: dependencies bijwerken

📝docs : correcte git clone link in readme

🔨fix (App.js): react router v6 syntax

🔨fix : restore pushState en replaceState

👽️ chore: import van @fluentui/react gewijzigd

⬆️ chore: dependencies bijwerken

## Version 3.1.0
🔖 chore: versie ophogen

🐛 fix: Zaak 864372 - bevinding office add-in verschijnt niet op het lint

⬆️ chore: dependencies bijwerken

⬆️ chore: dependencies bijwerken

⬆️  chore: dependencies ophogen

📌  chore: dependencies bijwerken

⬆️  chore: dependencies bijwerken

⬆️  chore: dependencies bijwerken

⬆️  chore: dependencies bijwerken

⬆️  chore: docker image bijwerken naar node 16

🚨  style: linter warnings wegwerken

🔥  chore: deprecated chrome extensie niet meer aanraden

👽️  chore: linter config bijwerken i.v.m. nieuwe versie

👷  ci: node image bijwerken

⬆️  chore: dependencies bijwerken

⬆️  chore: dependencies bijwerken

⬇️  chore: use node 14-bullseye

⬇️  chore: use node 14 on bullseye

⬆️  chore: use node 16

💚 ci: add sudo as dependency in build job

💚 ci: build job

💚 ci: pip not necessary when using GitLab AWS image

💚 ci: set different stage for prod deploy

🚀  build: docker image

💚 ci: remove only declaration on production job

🏗️  ci: use GitLab AWS image

👷  ci: update CI for AWS platform

👽️  chore: update webpack config for v4 dev server

➕  chore: update dependencies

📝 docs(readme): prod branch hernoemen

🐛 fix(webpack): geen trailing slash voor manifest

💬 feat: tekstwijziging i.v.m. requirement 1100.1.5

👽️ chore: imports bijwerken i.v.m. wijziging namespace @fluentui/react

⬆️  chore: dependencies bijwerken

📄 chore: jaartal ophogen

👷  ci: init

Merge branch 'outlook' into develop

🥅  feat: sentry integratie

🔖  chore: versie ophogen

✨ feat: Outlook ondersteuning

⬆️  chore: dependencies ophogen

➕  chore: toevoegen @typescript-eslint/parser als dependency

⬆️  chore: dependencies ophogen

🔒  chore: verhelpen npm advisory 1594 en 1589

➕  chore: toevoegen sentry als dependency

🔖 chore: versie ophogen

## Version 1.1.0
🔖  chore: versie ophogen

✨  feat: functionaliteit voor "afmelden"

## Version 1.0.1
🔖 chore: versie ophogen

🚑 fix(Middleware): altijd kleine slicesize gebruiken i.v.m. call stack limits

🔧 chore: juiste pad IconURL

## Version 1.0.0
🔖  chore: versie ophogen

⬆️  chore: dependencies ophogen

⬆️  chore: dependencies ophogen

🐛 fix: herstellen manifest

## Version 0.5.0
🔖 chore: versienummer ophogen

🐛 fix: Bevinding - Toevoegen document aan zaak - Documentnaam - Gebruik van aparte tekens

## Version 0.4.0
🔖 chore: ophogen versienummer

🐛 fix(manifest): juiste icon size, aanpassen requirement sets en toevoegen Workbook en Presentation host

🐛 fix(Middleware): juiste slice size op iOS

🚚  feat: cache busting

➕  chore: toevoegen path als dependency

## Version 0.3.0
🔖 chore: versie ophogen

🐛 fix: Bevinding - Nieuwe versie toevoegen - Nieuw document - Geen rechten

⬆️  chore: dependencies ophogen

🔧 chore: correcte file association voor react

🔥 style: Progress wordt maar één keer gebruikt en hoeft geen apart component te zijn

➕  chore: toevoegen react-is als dependency

🎨 style: handler functions zijn niet langer nodig met React Hooks API

🐛 fix: tikfoutje, caret op versienummer in plaats van packagenaam

⬆️ chore: dependencies bijwerken

👽 refactor: vervangen van class components met Hooks API

🐛🍱  fix: Suggestie - Icoontje

## Version 0.2.0
🔖 chore: versie ophogen

🐛 fix: Bevinding - Statusmelding

🐛 fix: Bevinding - Overschakelen naar versie? 🐛 fix: Bevinding - Toevoegen document aan zaak - Documentnaam - Gebruik van aparte tekens

🔧 chore: relatieve URL voor middleware

🐛 fix: Bevinding - Toevoegen document aan zaak - Geen documentnaam en documenttype

🐛 fix: Bevinding - Toevoegen document aan zaak - Geen documentnaam

🐛 fix: Bevinding - Inzoomen

🐛 fix: Bevinding - Statusmelding

🐛 fix: Bevinding / Suggestie - Feedback bij toevoegen codes

✨ feat: Suggestie - Nog geen gekoppelde account

🐛 fix: iOS ondersteunt slices van maximaal 64 kB

Merge branch 'license' into 'develop'

📄 chore: open licentie i.o.m. @Dylankamoen

## Version 0.1.0
🔖 chore: versienummer ophogen

🎉
